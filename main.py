# admin/manage.py


### Right now all relational ORM is done in the code - which probably won't scale
### to many digests. It would be nice to add an sql integration to do checks there
### and maybe add a RabbitMQ trigger for microservices communication.

from src import create_app

from flask_script import Manager

app = create_app()
manager = Manager(app)

@manager.command
def test_run():
    app.run(host='0.0.0.0', debug=True, port=8080)

if __name__ == '__main__':
    manager.run()
