from unittest.mock import patch
from flask.testing import FlaskClient
from src.test.fixtures import app, client

from .service import CategoryService
from .object  import Category, CategorySchema
from . import BASE_ROUTE

class TestCatResource:
    @patch.object(CategoryService, 'get_all', lambda x: [
        Category(0, 'Политика'), Category(1, 'Экономика')
    ])

    def test_get(self, client: FlaskClient):
        with client:

            results = client.get(f'/admin/{BASE_ROUTE}',
                                 follow_redirects=True).get_json()
            expected = CategorySchema(many=True).dump(
                [Category(0, 'Политика'),
                 Category(1, 'Экономика')]
            )
            for r in results:
                assert r in expected
