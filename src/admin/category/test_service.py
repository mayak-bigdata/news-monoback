import pytest

from .object import Category, CategoryInterface
from .service import CategoryService, NameExists

from src.test.fixtures import db

from pymongo.database import Database

def test_insert_get(db: Database):

    serv = CategoryService(db)

    item = CategoryInterface(
        name='Политика'
    )

    c = serv.create(item)
    created = serv.get(c.id)

    assert created.id == 0 and created.name == item['name']

def test_get_nonexistent(db: Database):

    serv = CategoryService(db)

    with pytest.raises(KeyError):
        serv.get(0)

def test_duplicates(db: Database):

    serv = CategoryService(db)

    item = CategoryInterface(
        name='Политика'
    )
    serv.create(item)

    with pytest.raises(NameExists):
        serv.create(item)
