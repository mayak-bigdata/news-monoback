from flask import request, abort
from flask_accepts import accepts, responds
from flask_restx import Namespace, Resource
from flask.wrappers import Response
from typing import List
from injector import inject


from .object import Category, CategorySchema, CategoryInterface
from .service import CategoryService, NameExists

## Declaration

api = Namespace(
    'Category',
    description='Responsible for management of categories'
    )


## Resources

@api.route('/')
class CategoryResource(Resource):
    '''Categories'''

    @inject
    def __init__(self, service: CategoryService, **kwargs):
        self.service = service
        super().__init__(**kwargs)

    @responds(schema=CategorySchema(many=True), api=api)
    def get(self) -> List[Category]:
        '''Get all categories'''
        return self.service.get_all()

    @accepts(
        model_name='Category',
        schema=CategorySchema,
        api=api
    )
    @responds(schema=CategorySchema, api=api)
    def post(self) -> Category:
        '''Create a Single category'''
        try:
            return self.service.create(request.parsed_obj)
        except NameExists:
            return abort(400, description='Category already exists')


@api.route('/<int:categoryId>')
@api.param('categoryId', 'Category database ID')
class CategoryIdResource(Resource):

    @inject
    def __init__(self, service: CategoryService, **kwargs):
        self.service = service
        super().__init__(**kwargs)

    @responds(schema=CategorySchema, api=api)
    def get(self, categoryId: int) -> Category:
        '''Get Single Category'''
        try:
            return self.service.get(categoryId)
        except KeyError:
            return abort(404, description='This category does not exist.')
