from mypy_extensions import TypedDict
from marshmallow import fields, Schema
from dataclasses import dataclass

### Model

@dataclass
class Category:
    id: int
    name: str

### Interface

class CategoryInterface(TypedDict, total=False):

    id: int
    name: str

### Schema

class CategorySchema(Schema):

    id = fields.Integer(readonly=True, description='The id')
    name = fields.String(required=True, description='The category name')
