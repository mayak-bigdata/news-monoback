# src/category/__init__.py

from .object import Category, CategorySchema
BASE_ROUTE = 'category'


def register_routes(api, app, root='api'):
    from .controller import api as category_api

    api.add_namespace(category_api, path=f'/{root}/{BASE_ROUTE}')
