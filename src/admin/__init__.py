from flask import Flask, jsonify, request
from flask_restx import Namespace
from pymongo import MongoClient
import pymongo

from src.admin.routes import register_routes

from src.admin.category.service import CategoryService
from src.admin.news_title.service import NewsTitleService
from src.admin.active_title.service import ActiveTitleService
from src.admin.digest.service import DigestService
from src.admin.sync.service import SyncService


class Admin:

    def __init__(self, services):
        self.services = services

def create(api, app, root='admin'):

    # Attach handlers

    register_routes(api, app, root=root)

    # Create services

    cl = MongoClient('mongodb+srv://recommender:pass@mayakreact-bbx0n.gcp.mongodb.net/recommender_db?retryWrites=true&w=majority')
    db = cl['recommender_db']

    cat_service  = (CategoryService, CategoryService(db))
    news_service = (NewsTitleService, NewsTitleService(db, cat_service[1]))
    act_service  = (ActiveTitleService, ActiveTitleService(news_service[1], cache_path=app.config.get('CACHE_PATH')))
    digest_service = (DigestService, DigestService(act_service[1]))
    sync_service = (SyncService, SyncService(
                                     cache_path=app.config.get('CACHE_PATH'),
                                     project_name=app.config.get('PROJECT_NAME'),
                                     bucket_name=app.config.get('BUCKET_NAME'),
                                     save_path=app.config.get('SAVE_PATH'),
                                     digest_serv=digest_service[1],
                                     active_serv=act_service[1],
                                     news_serv=news_service[1]
                   ))

    services = [cat_service, news_service, act_service, digest_service, sync_service]

    return Admin(services)
