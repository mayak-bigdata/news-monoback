# routes.py -- here all routing stuff is configured

def register_routes(api, app, root='api'):
    from src.admin.category import register_routes as attach_category
    from src.admin.news_title import register_routes as attach_news
    from src.admin.active_title import register_routes as attach_active
    from src.admin.digest import register_routes as attach_digest
    from src.admin.sync import register_routes as attach_sync
    # Add routes

    attach_category(api, app, root)
    attach_news(api, app, root)
    attach_active(api, app, root)
    attach_digest(api, app, root)
    attach_sync(api, app, root)
