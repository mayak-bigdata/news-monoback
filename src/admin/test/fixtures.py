import pytest
import os
import shutil

from src.config import Config
from src.test.fixtures import db

from src.admin.category.service import CategoryService

c = Config(env='test')

def rm_cache():
    if os.path.isdir(c.CACHE_PATH):
        shutil.rmtree(c.CACHE_PATH)

@pytest.fixture
def cache():
    rm_cache()
    os.makedirs(c.CACHE_PATH)

@pytest.fixture
def cat_service(db):
    return CategoryService(db)
