import pytest

from .object import NewsTitle, NewsTitleInterface
from .service import NewsTitleService, NameExists, NoCategory
from src.admin.category.object import CategoryInterface

from src.admin.test.fixtures import cat_service
from src.test.fixtures import db


def test_insert_get(db, cat_service):

    serv = NewsTitleService(db, cat_service)

    cat_id = cat_service.create(CategoryInterface(name='Криминал')).id

    item = NewsTitleInterface(
        name='Убили негра',
        text='Ойойой убили негра',
        cat=cat_id
    )

    item = serv.create(item)
    got = serv.get(item.id)

    assert item == got


def test_get_nonexistent(db, cat_service):

    serv = NewsTitleService(db, cat_service)

    cat_id = cat_service.create(CategoryInterface(name='Криминал')).id

    with pytest.raises(KeyError):
        serv.get(0)

def test_duplicates(db, cat_service):

    serv = NewsTitleService(db, cat_service)

    cat_id = cat_service.create(CategoryInterface(name='Криминал')).id

    item = NewsTitleInterface(
        name='Убили негра',
        text='Ойойой убили негра',
        cat=cat_id
    )

    serv.create(item)

    with pytest.raises(NameExists):
        serv.create(item)

def test_no_category(db, cat_service):

    serv = NewsTitleService(db, cat_service)

    item = NewsTitleInterface(
        name='Убили негра',
        text='Ойойой убили негра',
        cat=228
    )

    with pytest.raises(NoCategory):
        serv.create(item)
