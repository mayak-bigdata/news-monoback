from pymongo.database import Database
from typing import List
import os

from src.admin.category.service import CategoryService
from .object import NewsTitle, NewsTitleInterface

class NoCategory(Exception):
    pass

class NameExists(Exception):
    pass

## Titles management

class NewsTitleService():

    def __init__(self, db: Database, cat_service: CategoryService):
        self._db = db
        self.temp = {}
        self.count = 0
        self.cat_service = cat_service

    def get_num(self, num) -> List[NewsTitle]:
        return list(self.temp.values())[-num:]

    def get(self, id: int) -> NewsTitle:
        return self.temp[id]

    def has(self, id: int) -> bool:
        return id in self.temp

    def create(self, new: NewsTitleInterface) -> NewsTitle:

        if not self.cat_service.has( new['cat'] ):
            raise NoCategory

        if new['name'] in map(lambda x: x.name, self.temp.values()):
            raise NameExists
            
        id = self.count; self.count += 1

        new_obj = NewsTitle( id = id, **new )

        self.temp[id] = new_obj

        return self.temp[id]
