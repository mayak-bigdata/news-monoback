from unittest.mock import patch
from flask.testing import FlaskClient
from src.test.fixtures import app, client

from .service import NewsTitleService
from .object  import NewsTitle, NewsTitleSchema
from . import BASE_ROUTE

class TestNewsResource:
    @patch.object(NewsTitleService, 'get_num', lambda x, num: [
        NewsTitle(
            id=0,
            name='Убили негра',
            text='Ойойой убили негра',
            cat=0
        ),
        NewsTitle(
            id=1,
            name='ААА убили негра',
            text='Ойойой убили негра',
            cat=0
        )
    ])

    def test_get(self, client: FlaskClient):
        with client:

            results = client.get(f'/admin/{BASE_ROUTE}?num=2',
                                 follow_redirects=True).get_json()
            expected = NewsTitleSchema(many=True).dump(
                [NewsTitle(
                    id=0,
                    name='Убили негра',
                    text='Ойойой убили негра',
                    cat=0
                ),
                NewsTitle(
                    id=1,
                    name='ААА убили негра',
                    text='Ойойой убили негра',
                    cat=0
                )]
            )
            for r in results:
                assert r in expected
