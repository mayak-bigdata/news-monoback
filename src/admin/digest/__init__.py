# src/digest/__init__.py

BASE_ROUTE = 'digest'

def register_routes(api, app, root='api'):
    from .controller import api as digest_api

    api.add_namespace(digest_api, path=f'/{root}/{BASE_ROUTE}')
