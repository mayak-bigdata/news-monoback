from typing import List

from src.admin.active_title.service import ActiveTitleService
from .object import Digest, DigestInterface

class NoActive(Exception):
    def __init__(self, name: str):
        self.name = name

## Management of audio files

class DigestService():

    def __init__(self, active_service: ActiveTitleService):
        self.active_service = active_service
        self.active = {}

    def get_available(self) -> List[Digest]:
        return [self.active[x] for x in self.active]

    def has(self, name: str):
        return name in self.active

    def insert(self, new: DigestInterface):

        for x in new['contents']:
            if not self.active_service.has(x):
                raise NoActive(x)

        self.active[new['name']] = Digest(**new)

        return self.active[new['name']]

    def get(self, name: str) -> Digest:

        if not name in self.active:
            raise KeyError

        return self.active[name]

    def delete(self, name: str) -> str:
        self.active.pop(name, None)
        return name
