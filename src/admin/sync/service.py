import os
import shutil
import subprocess
from typing import List

from google.cloud import storage

from src.admin.digest.service import DigestService
from src.admin.active_title.service import ActiveTitleService
from src.admin.news_title.service import NewsTitleService
import src.admin.sync.audio as audio

class SyncService():

    def __init__(
        self,
        cache_path:  str,
        project_name: str,
        bucket_name: str,
        save_path: str,
        digest_serv: DigestService,
        active_serv: ActiveTitleService,
        news_serv:   NewsTitleService,
        ):

        self.cache_path  = os.path.join(cache_path, 'hls')

        self.digest_serv = digest_serv
        self.active_serv = active_serv
        self.news_serv   = news_serv
        self.state = []

        self.save_path = save_path
        self.client = storage.Client(project=project_name)
        self.bucket = self.client.get_bucket(bucket_name)

        # TODO: Here you must delete what has been left over from the last time.


    def upload_file(self, filename: str, target_name:str):

        blob = storage.Blob(target_name, self.bucket)

        with open(filename, "rb") as contents:
            blob.upload_from_file(contents)

        return target_name


    def update_state(self):

        # Create digests dictionary

        def insert_fields(news_id):
            files = self.active_serv.get(news_id)
            meta  = self.news_serv.get(news_id)
            return {
                'id': news_id,
                'name': meta.name,
                'text': meta.text,
                'audio_name': files.audio_name,
                'cat': meta.cat
            }

        insert_news = lambda x: {'name':x.name, 'contents':[insert_fields(x) for x in x.contents]}
        digests = list(map(insert_news, self.digest_serv.get_available()))

        # Upload files to cloud store

        if os.path.isdir(self.cache_path):
            shutil.rmtree(self.cache_path)

        os.makedirs(self.cache_path)

        for dig in digests:

            audio.concat_mp3(
                file_list = [ x['audio_name'] for x in dig['contents'] ],
                input_dir = self.active_serv.cache_path,
                output_dir = '.',
                output_name = 'temp.mp3'
            )
            audio.prepare_hls(
                input_file = 'temp.mp3',
                digest_name = dig['name'],
                hls_path = self.cache_path
            )
            os.remove('list.txt')
            os.remove('temp.mp3') #this will fuck up because of writing permissions

            # Now upload it to google cloud storage

            for file in os.listdir(os.path.join(self.cache_path, dig['name'])):

                orig_name = os.path.join(self.cache_path, dig['name'], file)
                target_name = os.path.join(self.save_path, dig['name'], file)

                self.upload_file(orig_name, target_name)

        # Creation itself

        self.state = digests

        return 0
