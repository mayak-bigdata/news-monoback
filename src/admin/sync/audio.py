import subprocess
import sys
import os
import json

## ffmpeg functions

def prepare_hls(input_file, digest_name, hls_path):

    dig_path = os.path.join(hls_path, digest_name)
    os.makedirs(dig_path, exist_ok=True)

    command = "ffmpeg \
      -i {} \
      -vn -ac 2 -acodec aac \
      -start_number 0 -hls_time 10 -hls_list_size 0 -f hls {}".format(input_file, os.path.join(dig_path, 'index.m3u8'))

    return subprocess.run(command.split(), check=True)

def concat_mp3(file_list, input_dir, output_dir, output_name):

    input_files = map(lambda x: os.path.join(input_dir, x), file_list)
    output_path = os.path.join(output_dir, output_name)

    with open('list.txt', 'w') as f:
        f.write('\n'.join(map(lambda x: 'file ' + "'{}'".format(x), input_files)))

    command = 'ffmpeg -f concat -i {} -acodec copy {}'.format('list.txt', output_path)

    return subprocess.run(command.split(), check=True)

## infrastructure functions

def get_config(path):
    with open(path) as json_file:
        conf = json.load(json_file)

    parts = {x['id']: x['file_name'] for x in conf['contents']}

    return parts, conf["digests"]

def initial_preparation(root_path):

    parts_paths, digests = get_config(os.path.join(root_path, 'digests.json'))

    parts_dir = os.path.join(root_path, 'parts')

    for dig in digests:
        concat_mp3([parts_paths[x] for x in dig["contents"]], parts_dir, '.', 'tmp.mp3')
        prepare_hls('tmp.mp3', dig['name'], 'data/digests/hls')
        os.remove('list.txt')
        os.remove('tmp.mp3')

    print("Processing is done!")

    # now we need to make a tar archive and then transfer it to the /mnt directory
    return 0

## main

if __name__ == "__main__":
    print("Preparing hls digests...")
    initial_preparation(sys.argv[1])
