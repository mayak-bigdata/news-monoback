from flask import request, abort, jsonify, send_file
from flask_restx import Namespace, Resource
from flask.wrappers import Response
from typing import List
from injector import inject

from .service import SyncService

api = Namespace('Sync', description='Syncronise state of the admin panel with an app.')

######
## System's state control
######

@api.route('/')
class StateResource(Resource):
    '''System state'''

    @inject
    def __init__(self, service: SyncService, **kwargs):
        self.service = service
        super().__init__(**kwargs)

    def get(self) -> List:
        '''Get text of current digests'''

        if not len(self.service.state):
            abort(400, 'Please refresh the state')

        return self.service.state

    def post(self) -> Response:
        '''Syncronise state of the app with current digests'''

        try:
            self.service.update_state()
        except:
            abort(500, 'Something went wrong')

        return dict(status='ok')
