from mypy_extensions import TypedDict
from marshmallow import fields, Schema
from dataclasses import dataclass


ALLOWED_EXTENSIONS = {'mp3'}

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

### Model

@dataclass
class ActiveTitle:

    news_id: int
    audio_name: str

### Interface

class ActiveTitleInterface(TypedDict, total=False):

    news_id: int
    audio_name: str

### Schema

class ActiveTitleSchema(Schema):

    news_id = fields.Integer(required=True, description='The news unique identifier')
    audio_name = fields.String(required=True, description='Name of the audio file')
