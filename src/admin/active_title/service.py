import os
from typing import List

from src.admin.news_title.service import NewsTitleService
from .object import ActiveTitle, ActiveTitleInterface

class NoNews(Exception):
    pass

## Management of audio files

class ActiveTitleService():

    def __init__(self, news_service: NewsTitleService, cache_path: str):
        self.news_service = news_service
        self.active = {}
        self.cache_path = os.path.join(cache_path, 'parts')

        if not os.path.isdir(self.cache_path):
            os.makedirs(self.cache_path)

    def get_available(self) -> List[int]:
        return list(self.active.keys())

    def has(self, id: int):
        return id in self.active

    def insert(self, new: ActiveTitleInterface) -> ActiveTitle:

        if not self.news_service.has(new['news_id']):
            raise NoNews

        if not os.path.exists( os.path.join(self.cache_path, new['audio_name']) ):
            raise FileNotFoundError

        self.active[new['news_id']] = ActiveTitle(**new)

        return self.active[new['news_id']]

    def get(self, id: int) -> ActiveTitle:

        if not id in self.active:
            raise KeyError

        return self.active[id]

    def delete(self, id: int) -> int:
        # TODO: delete file here
        # TODO: add unique id
        self.active.pop(id, None)
        return id
