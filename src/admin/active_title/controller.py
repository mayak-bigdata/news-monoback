import os
from flask import request, abort, jsonify
from flask_accepts import accepts, responds
from flask_restx import Namespace, Resource
from flask.wrappers import Response
from typing import List
from injector import inject
from werkzeug.utils import secure_filename


from .object import ActiveTitle, ActiveTitleSchema, \
                        ActiveTitleInterface, allowed_file

from .service import ActiveTitleService, NoNews

api = Namespace('ActiveTitle', description='Responsible for management for static title content - audio, images.')

######
## Text news control
######

@api.route('/')
class ActiveTitleResource(Resource):
    '''NewsTitles'''

    @inject
    def __init__(self, service: ActiveTitleService, **kwargs):
        self.service = service
        super().__init__(**kwargs)

    #@responds(schema=ActiveTitleSchema(many=True), api=api)
    def get(self) -> List[int]:
        '''Get last news added to the database'''
        return self.service.get_available()

    #@accepts(schema=ActiveTitleSchema, api=api)
    @responds(schema=ActiveTitleSchema, api=api)
    #@api.doc(consumes='multipart/form-data')
    def post(self) -> ActiveTitle:
        """Load static content for a title using multipart/form-data request."""
        try:

            if 'file' not in request.files:
                abort(400, description='No file was included')
            file = request.files['file']
            if file.filename == '' or not allowed_file(file.filename):
                abort(400, description='Please check your file')

            if 'news_id' not in request.form:
                abort(400, description='Enter news id')

            if file:

                filename = secure_filename(file.filename)
                file.save(os.path.join(self.service.cache_path, filename))

                new = ActiveTitleSchema().load(request.form)
                new['audio_name'] = filename

                return self.service.insert(new)

        except NoNews:
            abort(404, description='This news title does not exist.')


### Control of specifics title data

@api.route('/<int:titleId>')
@api.param('titleId', 'NewsTitle database ID')
class ActiveTitleIdResource(Resource):

    @inject
    def __init__(self, service: ActiveTitleService, **kwargs):
        self.service = service
        super().__init__(**kwargs)

    @responds(schema=ActiveTitleSchema, api=api)
    def get(self, titleId: int) -> ActiveTitle:
        '''Get contents of one single title'''

        try:
            return self.service.get(titleId)
        except KeyError:
            abort(404, description='Data was not found.')

    def delete(self, titleId: int) -> Response:
        '''Delete content of one single title'''
        id = self.service.delete(titleId)
        return jsonify(dict(status='ok', id=id)), 200
