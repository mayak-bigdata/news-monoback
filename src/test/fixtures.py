import pytest
from pymongo import MongoClient
from src import create_app

@pytest.fixture
def db():
    cl = MongoClient('mongodb+srv://recommender:pass@mayakreact-bbx0n.gcp.mongodb.net/recommender_db?retryWrites=true&w=majority')
    return cl['recommender_db']

@pytest.fixture
def app():
    return create_app(env='test')

@pytest.fixture
def client(app):
    return app.test_client()
