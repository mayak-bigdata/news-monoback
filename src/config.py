# admin/src/config.py

import os

class Config(object):

    def __init__(self, env):

        if env == 'test':
            self.CACHE_PATH = 'cache'
            self.PROJECT_NAME = 'mayaknews'
            self.BUCKET_NAME = 'mayaknews'
            self.SAVE_PATH = 'hls'

        elif env == 'staging':
            self.CACHE_PATH = os.environ.get('CACHE_PATH')
            self.PROJECT_NAME = os.environ.get('PROJECT_NAME')
            self.BUCKET_NAME = os.environ.get('BUCKET_NAME')
            self.SAVE_PATH = os.environ.get('SAVE_PATH')

        JSON_AS_ASCII = False
