from flask import Flask, jsonify, request
from flask_restx import Api
from flask_injector import FlaskInjector

from src.config import Config

from src.admin import create as create_admin

# App Declaration

def create_app(env='test'):

    # Declaration

    app = Flask(__name__)
    app.config.from_object(Config(env))

    api = Api(app, title="Application main", version="0.1.0")

    @app.route("/")
    def health():
        return jsonify("ok")

    admin = create_admin(api, app, 'admin')

    FlaskInjector(
        app=app,
        modules=[bind_service(x) for x in admin.services]
    )

    return app

def bind_service(service):
    def bind(binder):
        binder.bind(
            service[0],
            to=service[1]
        )
    return bind
